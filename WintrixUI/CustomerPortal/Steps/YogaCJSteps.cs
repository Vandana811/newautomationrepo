﻿using System;
using TechTalk.SpecFlow;

namespace IgniteAutomationTests.WintrixUI.CustomerPortal.Steps
{
    [Binding]
    public class YogaCJSteps
    {
        [Given(@"I have entered into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press")]
        public void WhenIPress()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the result  be (.*) on the screen")]
        public void ThenTheResultBeOnTheScreen(int p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
